package frame_bank;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


public class FrameBankAccount extends JFrame {
	private BankAccount bank;
	private JPanel panel,panel2,panel3,panel4;
	private JTextField inputw,inputd;
	private JButton withdraw,deposit;
	private JLabel output;
	private static final int WIDTH_FIELD = 10;
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 250;
	private String str = "";
	
	public static void main(String[] args){
		new FrameBankAccount();
	}
	
	public FrameBankAccount(){
		bank = new BankAccount("black");
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(400,150);
	}
	public void createPanel(){
		inputw = new JTextField(WIDTH_FIELD);
		inputd = new JTextField(WIDTH_FIELD);
		withdraw = new JButton("withdraw");
		deposit = new JButton("deposit");
		output = new JLabel(str);
		panel = new JPanel();
		panel2 = new JPanel();
		panel2.add(inputd);
		panel2.add(deposit);
		panel2.setBorder(new TitledBorder(new EtchedBorder(),"Deposit"));
		panel3 = new JPanel();
		panel3.add(inputw);
		panel3.add(withdraw);
		panel3.setBorder(new TitledBorder(new EtchedBorder(),"Withdraw"));
		panel4 = new JPanel();
		panel4.add(output);
		panel4.setBorder(new TitledBorder(new EtchedBorder(),"Balance in your bank"));
		panel.setLayout(new GridLayout(3,1));
		panel.add(panel2);
		panel.add(panel3);
		panel.add(panel4);
		add(panel,BorderLayout.CENTER);
		
		deposit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bank.deposit(Integer.parseInt(inputd.getText()));
				setResult(bank.toString());
				setButton();
			}
		});
		
		withdraw.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(Integer.parseInt(inputw.getText())>bank.getBalance()){
					setResult("Insufficient funds,please try again");
					setButton();
				}
				else{
					bank.withdraw(Integer.parseInt(inputw.getText()));
					setResult(bank.toString());
					setButton();
				}
			}
		});
		
	}
	public void setResult(String s){
		str = s;
		output.setText(str);
	}
	
	public void setButton(){
		inputw.setText("");
		inputd.setText("");
	}
	
}
