package frame_1_5;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

	
public class FrameComboBox extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colerPanel,panel;
	private JComboBox combo;
	
	public static void main(String[] args){
		new FrameComboBox();
	}
	public FrameComboBox(){
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(400,150);
	}
	public void createPanel(){
		colerPanel = new JPanel();
		colerPanel.setBackground(new Color(255,46,46));
		add(colerPanel,BorderLayout.CENTER);
		
		panel = new JPanel();
		combo = new JComboBox();
		combo.addItem("Red");
		combo.addItem("Green");
		combo.addItem("Blue");
		combo.setEditable(true);
		panel.add(combo);
		combo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(combo.getSelectedItem().equals("Red")){
					colerPanel.setBackground(new Color(255,46,46));
				}
				else if(combo.getSelectedItem().equals("Green")){
					colerPanel.setBackground(new Color(25,255,52));
				}
				else{
					colerPanel.setBackground(new Color(12,29,255));
				}
			}
		});
		
		add(panel,BorderLayout.SOUTH);
	}
}
