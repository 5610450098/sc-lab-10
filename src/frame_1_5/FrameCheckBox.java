package frame_1_5;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class FrameCheckBox extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colerPanel,panel;
	private JCheckBox red,green,blue;
	private ActionListener list;
	
	public static void main(String[] args){
		new FrameCheckBox();
	}
	public FrameCheckBox(){
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(400,150);
		list = new addActionListener();
		red.addActionListener(list);
		green.addActionListener(list);
		blue.addActionListener(list);
	}
	public void createPanel(){
		colerPanel = new JPanel();
		colerPanel.setBackground(new Color(255,255,255));
		add(colerPanel,BorderLayout.CENTER);
		
		panel = new JPanel();
		red = new JCheckBox("Red");
		green = new JCheckBox("Green");
		blue = new JCheckBox("Blue");
		panel.add(red);
		panel.add(green);
		panel.add(blue);
		add(panel,BorderLayout.SOUTH);
	}
	
	class addActionListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			setColor();
		}
	}
	public void setColor(){
		Color c = new Color(255,255,255);
		if(red.isSelected()){
			c = new Color(255,46,46);
		}
		else if(blue.isSelected()){
			c = new Color(12,29,255);
		}
		else if(green.isSelected()){
			c = new Color(25,255,52);
		}
		if(red.isSelected()&&blue.isSelected()){
			c = new Color(224,50,255);
		}
		if(green.isSelected()&&blue.isSelected()){
			c = new Color(0,255,255);
		}
		if(red.isSelected()&&green.isSelected()){
			c = new Color(252,249,34);
		}
		if(red.isSelected()&&blue.isSelected()&&green.isSelected()){
			c = new Color(255,255,255);
		}
		colerPanel.setBackground(c);
	}
	
}
