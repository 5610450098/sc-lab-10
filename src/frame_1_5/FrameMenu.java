package frame_1_5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class FrameMenu extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JMenu color;
	private JMenuItem red,green,blue;
	private JMenuBar bar;
	private JPanel colerPanel;
	
	public static void main(String[] args){
		new FrameMenu();
	}
	public FrameMenu(){
		createMenu();
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(400,150);
	}
	public void createPanel(){
		colerPanel = new JPanel();
		colerPanel.setBackground(new Color(255,255,255));
		add(colerPanel,BorderLayout.CENTER);
	}
	
	public void createMenu(){
		color = new JMenu("Color");
		red = new JMenuItem("red");
		green = new JMenuItem("green");
		blue = new JMenuItem("blue");
		
		color.add(red);
		color.add(green);
		color.add(blue);
		
		red.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				colerPanel.setBackground(new Color(255,46,46));
			}
		});
		
		green.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				colerPanel.setBackground(new Color(25,255,52));
			}
		});
		blue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				colerPanel.setBackground(new Color(12,29,255));
			}
		});
		
		bar = new JMenuBar();
		setJMenuBar(bar);
		bar.add(color);
	}
}
