package frame_1_5;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class FrameButton extends JFrame {
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	private JPanel colerPanel,button;
	private JButton red,green,blue;
	
	public static void main(String[] args){
		new FrameButton();
	}
	public FrameButton(){
		createPanel();
		setSize(FRAME_WIDTH,FRAME_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLocation(400,150);
	}
	public void createPanel(){
		colerPanel = new JPanel();
		colerPanel.setBackground(new Color(255,46,46));
		add(colerPanel,BorderLayout.CENTER);
		
		button = new JPanel();
		red = new JButton("Red");
		green = new JButton("Green");
		blue = new JButton("Blue");
		button.add(red);
		button.add(green);
		button.add(blue);
		add(button,BorderLayout.SOUTH);
		
		class RedListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colerPanel.setBackground(new Color(255,46,46));
			}
		}
		class GreenListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colerPanel.setBackground(new Color(25,255,52));
			}
		}
		class BlueListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				colerPanel.setBackground(new Color(12,29,255));
			}
		}
		
		red.addActionListener(new RedListener());
		green.addActionListener(new GreenListener());
		blue.addActionListener(new BlueListener());
	}
}
